# Bootstrapping DDKv2

This project contains code to bootstrap DDKv2 development.

## Quick Start

* Using sources from local Kleaf repo checkout.

```shell
python3 init.py  --local --kleaf_repo=/abs/path/to/ACK
```

* Using local prebuilts with local sources.

```shell
python3 init.py --prebuilts_dir=/abs/path/to/prebuilts  --local --kleaf_repo=/abs/path/to/ACK
```

